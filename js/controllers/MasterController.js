app.controller('MasterController', ['$scope', 'localStorageService', function($scope, localStorageService){
	$scope.dataTask = {};
	$scope.sortingLog = [];
	var tmpList = [];
	var modal = $('#add-task');

	if(localStorageService.get('angular-todo-list')){
		$scope.todos = localStorageService.get('angular-todo-list');
	}else{
		$scope.todos = [[],[],[], []];
	}
	
	$scope.$watchCollection('todos[0]', function(){
		console.log('se modifico el todo');
		localStorageService.set('angular-todo-list', $scope.todos);
	});

	$scope.$watchCollection('todos[1]', function(){
		console.log('se modifico el todo');
		localStorageService.set('angular-todo-list', $scope.todos);
	});

	$scope.$watchCollection('todos[2]', function(){
		console.log('se modifico el todo');
		localStorageService.set('angular-todo-list', $scope.todos);
	});

	$scope.$watchCollection('todos[3]', function(){
		console.log('se modifico el todo');
		localStorageService.set('angular-todo-list', $scope.todos);
	});

	$scope.addTask = function(){
		$scope.todos[$scope.dataTask.index].push($scope.dataTask);
		clearData();
		closeModal();
	}

	$scope.removeTask = function(index){
		$scope.todos.splice(index, 1);
	}

	function clearData(){
		$scope.taskForm.$submitted = false;
		$scope.dataTask = {};
	}

	function createOptions (listName) {
	    var _listName = listName;
	    var options = {
	    	placeholder: "app",
	    	connectWith: ".Task",
	      	activate: function() {
	        	console.log("list " + _listName + ": activate");
	      	},
	      	beforeStop: function() {
	          	console.log("list " + _listName + ": beforeStop");
	      	},
	      	change: function() {
	          	console.log("list " + _listName + ": change");
	      	},
	      	create: function() {
	          	console.log("list " + _listName + ": create");
	      	},
	      	deactivate: function() {
	          	console.log("list " + _listName + ": deactivate");
	      	},
	      	out: function() {
	          	console.log("list " + _listName + ": out");
	      	},
	      	over: function() {
	          	console.log("list " + _listName + ": over");
	      	},
	      	receive: function() {
	          	console.log("list " + _listName + ": receive");
	      	},
	      	remove: function() {
	          	console.log("list " + _listName + ": remove");
	      	},
	      	sort: function() {
	          	console.log("list " + _listName + ": sort");
	      	},
	      	start: function() {
	          	console.log("list " + _listName + ": start");
	      	},
	      	stop: function() {
	          	console.log("list " + _listName + ": stop");
	      	},
	      	update: function() {
	          	console.log("list " + _listName + ": update");
	      	}
	    };
	    return options;
	}

	$scope.sortableOptionsList = [
		createOptions('A'), 
		createOptions('B'),
		createOptions('C'),
		createOptions('D')
	];


  	$scope.createTask = function(index){
  		$scope.dataTask.index = index;
  		openModal();
  	};

  	function openModal(){
  		modal.openModal();
  	}

  	function closeModal(){
  		modal.closeModal();
  	}

}]);